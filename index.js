const fetch = require('node-fetch');

const levels = {
    log: 'INFO',
    info: 'INFO',
    error: 'ERROR',
    debug: 'DEBUG',
    warn: 'WARNING',
}

module.exports = (app, url = 'https://blacksheep-api-test.herokuapp.com/log') => {
    console.log('Starting logger...')
    Object.entries(levels)
        .forEach(([field, level]) => {
            const curr = console[field]
            console[field] = function log() {
                fetch(url, {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({
                        app,
                        level,
                        device: typeof device === 'object' ? device : {},
                        message: arguments,
                    }),
                })
                curr.call(console, ...arguments)
            }
        })
}
